INTRO
-----

This is for the paper: **Assessing the gene regulatory properties of Argonaute-bound small RNAs of diverse genomic origin** by **Thomson, D.W. et al**. 

[Online Early Release Link](http://nar.oxfordjournals.org/content/early/2014/12/01/nar.gku1242.full)

Data can soon be found here: <LINK TO SRA to come, E.G. http://www.ncbi.nlm.nih.gov/bioproject/257235>

NOTES
-----

The analyses described below will require aroound 100GB of free disc space and approx 8 hours running time (using 4 cores).

INSTALL
-------

The following are the commands used on a Fresh Ubuntu 14.04 installation:

	
```
#!bash

sudo apt-get update

# Install bwa, samtools, bowtie2, prinseq, etc
sudo apt-get install git bwa python-pip samtools bowtie2 python-dev gfortran libopenblas-dev liblapack-dev zlib1g-dev libblas-dev libatlas-base-dev libpng-dev libfreetype6-dev libpq-dev libxft-dev ghostscript

# Make sure numpy is installed properly before other components
sudo pip install numpy 

# Make a directory for this project and change into it
mkdir ~/Thomson_NAR
cd ~/Thomson_NAR
export PROJECT_DIR=`pwd`
	
# Get  PRINSEQ-lite
wget http://sourceforge.net/projects/prinseq/files/standalone/prinseq-lite-0.20.4.tar.gz
tar -xvf prinseq-lite-0.20.4.tar.gz
export PRINSEQ_DIR=${PROJECT_DIR}/prinseq-lite-0.20.4
```

INSTALL REFERENCES
------------------

You need to install the Illumina iGenomes package (which you may already have) and this projects raw sequencing data. You will also need to set an environment variable pointing to the iGenomes package.

```
#!bash

# If you don't already have the iGenomes package
# Go back to the project folder if you are not already there
cd ${PROJECT_DIR}

# Bring down and unpack the iGenomes Homo_sapiens directory here.
wget ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Homo_sapiens/UCSC/hg19/Homo_sapiens_UCSC_hg19.tar.gz
wget ftp://igenome:G3nom3s4u@ussd-ftp.illumina.com/Homo_sapiens/Ensembl/GRCh37/Homo_sapiens_Ensembl_GRCh37.tar.gz
tar xvfz Homo_sapiens_UCSC_hg19.tar.gz
tar xvfz Homo_sapiens_Ensembl_GRCh37.tar.gz

# Remove Sequence and GenomeStudio directories if you need the disk space
#rm -rf Homo_sapiens/Ensembl/GRCh37/Sequence Homo_sapiens/Ensembl/GRCh37/GenomeStudio

# Add iGenomes package location to your path for this session. If you placed the iGenomes directory somewhere different to above, modify the following line appropriately
export IGENOMES_DIR=${PROJECT_DIR}

# If you already have the iGenomes package, add it's location to your path for this session as shown on the following line and ignore all the above commands
#export IGENOMES_DIR=</path/to/your/local/copy/of/iGenomes>

# Either way, you will also need the raw sequencing files (which can be retrieved from GEO) in the project directory
# this stuff to come once the data has been uploaded to GEO
```



CLONE REPOSITORY
----------------

Clone the repository for this project and set up your path

```
#!bash

cd ${PROJECT_DIR}
git clone <2014_thomson.git>
cd 2014_thomson_nar
sudo pip install -r requirements.txt

# Unpack gtfs from repository into appropriate iGenomes directory
mv miRNA.hg19.gff ${IGENOMES_DIR}/Homo_sapiens/UCSC/hg19/Annotation/SmallRNA
mv tRNA.hg19.gtf ${IGENOMES_DIR}/Homo_sapiens/UCSC/hg19/Annotation/SmallRNA
mv tRNA.GRCh37.gtf ${IGENOMES_DIR}/Homo_sapiens/Ensembl/GRCh37/Annotation/SmallRNA

export PYTHONPATH=${PROJECT_DIR}/2014_thomson_nar
export PATH=$PATH:${PROJECT_DIR}/2014_thomson_nar/bioinformatics
```

RUNNING
-------


```
#!bash

# Use this to check that the IGENOMES_DIR variable still exists in your session. If nothing is printed to screen all is good.
if [ -z ${IGENOMES_DIR} ]; then
    echo "You must set the IGENOMES_DIR variable" >&2
    exit 1;
fi

# Make sure your in the project directory again
cd ${PROJECT_DIR}

# Modify run parameters at top of this script if desired. Must have at least 2 cores available, default: Use 4 cores.
thomson_2014_pipeline.sh $IGENOMES_DIR
	
# Plots and data files will be in the "analyses" directory
	
# To analyse public datasets from Burroughs et al (2011) and Flores et al (2014):
# For Burroughs data:
# Obtain and combine fastq files from Short Read Archive for samples DRS000281, DRS000282, DRS000283
# Process the fastq as shown below:
	
thomson_process_public_data.sh $IGENOMES_DIR <fastq> TCGTATGCCGTCTTCTGCTTGAAAAAAAAAAA
	
# For Flores data:
# Obtain fastq files for GEO study GSE58127 (samples GSM1401415, GSM1401417, GSM1401419, GSM1401421) from Short Read Archive.
# For each fastq file, process the data as shown in the example below:
	
thomson_process_public_data.sh $IGENOMES_DIR <fastq> TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC
```