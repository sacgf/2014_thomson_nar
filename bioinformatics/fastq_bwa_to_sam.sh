#!/bin/bash

FASTQ=${1}
CORES=${2}
IGENOMES_DIR=${IGENOMES_DIR}

if [ -z ${FASTQ} ]; then
        echo "Usage $(basename $0): <fastq> <# cores>. No fastq file provided as argument" >&2
        exit;
elif [ ! -e ${FASTQ} ]; then
        echo "$(basename $0): No fastq file ('${FASTQ}') found"
        exit
else
        echo "FASTQ = '${FASTQ}'";
fi

if [ -z ${CORES} ]; then
	echo "$(basename $0): No cores supplied - defaulting to using 4 cores."
	CORES=4
else
	echo "Using ${CORES} cores"
fi	

NAME=${FASTQ%.fastq}.bwa
BWA_INDEX=${IGENOMES_DIR}/Homo_sapiens/UCSC/hg19/Sequence/BWAIndex/genome.fa
echo "BWA Index = ${BWA_INDEX}"

# align reads with bwa
bwa aln -t ${CORES} ${BWA_INDEX} ${FASTQ} -f ${NAME}.sai

# generate sam alignment file
bwa samse -n 3 ${BWA_INDEX} ${NAME}.sai ${FASTQ} -f ${NAME}.sam

# Cleanup intermediate file
rm ${NAME}.sai
