#!/usr/bin/env python
'''
Created on 22/05/2013
@author: katherine
Removes a given sequence from 3' end of read only.  
Either trims perfect match at 3' end or, if no perfect match, leaves read untouched.
Intended to remove CCA from ends of tRNA reads.
'''
from argparse import ArgumentParser
from sacgf.util import file_utils, int_with_commas
import HTSeq

def handle_args():
    parser = ArgumentParser(description="Remove given seq from 3' end of reads")
    parser.add_argument("--fastq", required=True, help="fastq file")
    parser.add_argument("--seq", required=True, type=str, help="seq to remove from 3' end (str)")
    return parser.parse_args()

args = handle_args()
seq_to_trim = args.seq
fastq_reader = HTSeq.FastqReader(args.fastq, qual_scale='phred')

#write separate files for trimmed and untrimmed reads
sample_name = file_utils.remove_gz_if_exists(args.fastq)
sample_name = file_utils.name_from_file_name(sample_name)
trimmed_file = open("%s_trimmed.fastq" % sample_name, "w")
untrimmed_file = open("%s_untrimmed.fastq" % sample_name, "w")

a = 0
trimmed_reads = []
untrimmed_reads = []
for i, read in enumerate(fastq_reader):
    if read.seq[-3:] == seq_to_trim:
        read = read[:-3]
        read.name = read.name[:-6] #NOTE: This is necessary because HTSeq silently adds '[part]' to the end the name of the trimmed read
        read.write_to_fastq_file(trimmed_file)
        a += 1
    else:
        read.write_to_fastq_file(untrimmed_file)

trimmed_file.close()
untrimmed_file.close()
print "Of the %s reads in the original file, %s were trimmed to remove %s" % (int_with_commas(i + 1), int_with_commas(a), seq_to_trim)
