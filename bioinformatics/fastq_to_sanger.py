#!/usr/bin/env python

'''

SA Cancer Genomics Facility

Nov 1, 2012 - Initial Version

Based on http://www.biopython.org/DIST/docs/api/Bio.SeqIO.QualityIO.FastqPhredWriter-class.html

@author: dlawrence
'''

from Bio import SeqIO
import sys
from sacgf import genomics, util

def main(in_fastq, in_format):
    open_func = genomics.get_open_func(in_fastq)
    extension = genomics.get_sequence_extension(in_fastq)

    in_file = open_func(in_fastq)
    record_iterator = SeqIO.parse(in_file, in_format)
    name = genomics.get_sequence_name(in_fastq)
    out_file = open_func("%s.sanger%s" % (name, extension), "w")
    SeqIO.write(record_iterator, out_file, "fastq")

    in_file.close()
    out_file.close()

if __name__ == '__main__':
    util.require_args("fastq", "format (eg 'fastq-illumina')")
    main(sys.argv[1], sys.argv[2])
    