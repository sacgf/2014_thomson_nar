"""Genomic Annotations, Objects, and utilities"""

from Bio import SeqIO
import HTSeq
import os
import gzip
from sacgf.util import file_utils

class Reader_format_chrom(object):
    reader_clazz = None
    
    def __init__(self, alignment_filename, want_chr):
        self.reader = self.reader_clazz(alignment_filename)
        self.has_chr = sam_or_bam_has_chrom(self.reader)
        self.want_chr = want_chr
        
        if self.has_chr == self.want_chr: #Use standard reader class if possible
            self.iter_method = self.reader.__iter__
            self.getitem_method = self.reader.__getitem__
        else:
            self.iter_method = self._format_chrom_iter
            self.getitem_method = self._format_chrom_getitem
            
    def _format_chrom_iter(self):
        for aln in self.reader:
            if aln.aligned:
                aln.iv = iv_format_chrom(aln.iv, self.want_chr)
            yield aln
    
    def _format_chrom_getitem(self, iv):
        iv = iv_format_chrom(iv, self.has_chr) # to not get ValueError
        for aln in self.reader[iv]:
            if aln.aligned:
                aln.iv.chrom = format_chrom(aln.iv.chrom, self.want_chr)
            yield aln

    def __iter__(self):
        return self.iter_method()
        
    def __getitem__(self, iv):
        return self.getitem_method(iv)
        
class BAM_Reader_format_chrom(Reader_format_chrom):
    ''' Formats alignments to have/not have 'chr' in chromosome ID
        file name
        Allows random access
        want_chr: Boolean - whether you want "chr" at the beginning of chrom
        return: yields formatted alignment 
    '''
    reader_clazz = HTSeq.BAM_Reader

def sam_or_bam_has_chrom(sam_or_bam):
    ''' return true if sam or bam has chr in header reference names
        warning: this will die if you have BOTH chr and NO chr
    '''
    header_dict = sam_or_bam.get_header_dict()
    
    sq = header_dict['SQ']
    has_chr = 0
    no_chr = 0
    for row in sq:
        chrom = row['SN']
        if chrom.startswith('chr'):
            has_chr += 1
        else:
            no_chr += 1
            
    if has_chr and no_chr:
        raise ValueError("Header has both starting with 'chr' and non-starting with 'chr' reference name!")
    
    return bool(has_chr)

def format_chrom(chrom, want_chr):
    ''' Pass in a chromosome (unknown format), return in your format
        @param chrom: chromosome ID (eg 1 or 'chr1')
        @param want_chr: Boolean - whether you want "chr" at the beginning of chrom
        @return: "chr1" or "1" (for want_chr True/False) 
    '''
    if want_chr:   
        if chrom.startswith("chr"):
            return chrom
        else:
            return "chr%s" % chrom
    else:
        return chrom.replace("chr", "")

def iv_format_chrom(iv, want_chr):
    iv = iv.copy()
    iv.chrom = format_chrom(iv.chrom, want_chr)
    return iv

def HTSeqInterval_to_hash(iv):
    return {'chr' : iv.chrom, 'start' : iv.start, 'stop' : iv.end, 'strand' : iv.strand}

def iv_from_pos_and_offsets(g_pos, offset1, offset2):
    '''offset1 and offset2 are stranded distance from g_pos
    + = offset in 3' direction'''
    if g_pos.strand == '+':
        start = g_pos.pos + min([offset1, offset2])
        end = g_pos.pos + max([offset1, offset2])
    if g_pos.strand == '-':
        start = g_pos.pos - max([offset1, offset2]) + 1
        end = g_pos.pos - min([offset1, offset2]) + 1
    return HTSeq.GenomicInterval(g_pos.chrom, start, end, g_pos.strand)    

def fasta_to_hash(fasta):
    indexed_fasta = {}
    for record in SeqIO.parse(fasta, "fasta"):
        indexed_fasta[record.id] = str(record.seq)
    return indexed_fasta

def features_to_genomic_array_of_sets(feature_iterator, **kwargs):
    '''By default, the features must be stranded. Can set this via kwargs 'stranded' as False.'''
    is_stranded = kwargs.get("stranded", True)
    
    genomic_array_of_features = HTSeq.GenomicArrayOfSets("auto", stranded=is_stranded)
    
    for feature in feature_iterator:
        genomic_array_of_features[feature.iv] += feature
    return genomic_array_of_features

def get_unique_features_from_genomic_array_of_sets_iv(genomic_array_of_sets, iv):
    '''Collapse genomic array of sets into a unique list of the values in that region'''
    all_values = set()
    for iv, values_in_iv in genomic_array_of_sets[iv].steps():
        all_values.update(values_in_iv)
    return all_values

def iv_format(iv):
    return "%s:%d-%d" % (iv.chrom, iv.start, iv.end)

def get_sequence_extension(file_name):
    (name, extension) = os.path.splitext(file_name)
    if extension == ".gz":
        extension = os.path.splitext(name)[1] + extension
    return extension

def get_open_func(file_name):
    if file_name.endswith(".gz"):
        return gzip.open
    else:
        return open

def get_sequence_name(filename):
    filename = file_utils.remove_gz_if_exists(filename)
    return os.path.splitext(os.path.basename(filename))[0]


