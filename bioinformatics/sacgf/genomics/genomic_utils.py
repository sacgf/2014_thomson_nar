"""Genomic utilities"""
from sacgf.genomics import get_sequence_extension, BAM_Reader_format_chrom

def load_alignment_reader(file_name, want_chr):
    ''' Returns a reader which you can iterate over based on file extension
        BAM can be randomly accessed
    '''
    readers = {'.bam' : BAM_Reader_format_chrom}
    extension = get_sequence_extension(file_name)
    clazz = readers.get(extension)
    if not clazz:
        raise ValueError("Unknown alignment format %s" % extension)
    return clazz(file_name, want_chr=want_chr)
