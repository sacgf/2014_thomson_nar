'''
Represents a Gene (ie a human readable name for >=1 (overlapping) transcripts)

Created on Dec 31, 2012

@author: dlawrence
'''

import logging

class Gene(object):
    '''
    This represents a gene (ie a collection of transcript instances)
    '''
    ALL_BIOTYPES = "ALL_BIOTYPES"
    def __init__(self, name, transcripts, **kwargs):
        ''' name : Gene Name
            transcripts : list of transcripts
            biotype : (optional) transcripts/gene is only of this biotype (default = ALL_BIOTYPES)
        '''
        self.name = name
        self.biotype = kwargs.get("biotype", Gene.ALL_BIOTYPES)
        self.transcripts = transcripts
        self.has_coding_transcript = False
        iv = None
        non_overlapping = 0
        for t in transcripts:
            self.has_coding_transcript |= t.is_coding # If any transcript, set to true
            if iv is None:
                iv = t.iv.copy()
            elif iv.overlaps(t.iv):
                iv.extend_to_include(t.iv)
            else:
                non_overlapping += 1
        
        if non_overlapping:
            logger = logging.getLogger(__name__)
            logger.warn("%s has %d non-overlapping transcripts", name, non_overlapping)

        self.iv = iv
