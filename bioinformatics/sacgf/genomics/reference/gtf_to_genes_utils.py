'''
 
See: http://gtf-to-genes.googlecode.com/hg/doc/build/html/index.html for gtf_to_genes documentation
 
gtf_to_genes implementation note:
 
It would be maybe 5 seconds quicker to load genes in gtf_to_genes, and then construct our Genes/Transcript objects from that.
 
However, the Transcript object and calling code (eg users of features_by_type[]) rely on exons etc being
GenomicFeature objects, and it is too much work to refactor this out at the moment.
 
The current implementation is thus to convert BACK to GenomicFeature objects and do the swap-out for straight GTF
loading in reference.transcript method.
 
TODO: Abstract away exons etc as something else other than GenomicFeatures from client code, so we can make this change
 
Created on May 8, 2013
 
@author: dave
'''
 
from sacgf.genomics.reference.transcript import Transcript

class TranscriptIdBioTyper(object):
    ''' Sets 'biotype' feature attribute to "protein_coding" or "non_coding"
        based on whether transcript_id starts with "NM_" or "NR_" '''
     
    biotypes = {"NM_" : "protein_coding", "NR_" : "non_coding"}
    def __init__(self, feature_attribute="transcript_id"):
        self.feature_attribute = feature_attribute
 
    def set_biotype(self, feature):
        transcript_id = feature.attr[self.feature_attribute]
        biotype = self.get_biotype(transcript_id)
        feature.attr[Transcript.BIOTYPE] = biotype
 
    def get_biotype(self, transcript_id):
        for (start, biotype) in TranscriptIdBioTyper.biotypes.iteritems():
            if transcript_id.startswith(start):
                return biotype
             
        if "tRNA" in transcript_id:
            return "tRNA"
         
        raise ValueError("Could not determine biotype for %s" % transcript_id)
 
    def biotype_assignment_method(self):
        return "Using transcript_id to assign protein_coding/non_coding"
     

class HardcodedBioTyper(object):
    def __init__(self, biotype):
        self.biotype = biotype
 
    def set_biotype(self, feature):
        feature.attr[Transcript.BIOTYPE] = self.biotype
 
    def biotype_assignment_method(self):
        return "Hardcoded to %s" % self.biotype
