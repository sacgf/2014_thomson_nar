'''
Utilities to handle Genome Annotations and reference genomes

Because of the large size of the files (500mb genes GTF and >3 gig reference genome) it loads
required files on demand at first access.

Created on Nov 29, 2012

@author: dlawrence
'''

import HTSeq
from collections import defaultdict
import logging
import os

from sacgf import util
from sacgf import genomics
from sacgf.genomics.reference import gtf_to_genes_utils
from sacgf.genomics.reference.gene import Gene
from sacgf.genomics.reference.transcript import Transcript
from sacgf.util import config
from sacgf.util.lazy import Lazy


HUMAN_CHROMOSOMES = set([str(i) for i in range(1, 23)] + ["X", "Y"]) # Now use 'X' instead of 'chrX'

class Reference(object):
    TRNA_BIOTYPE = "tRNA"
    DEFAULT_GENE_IDENTIFIER = "gene_name"
    DEFAULT_TRANSCRIPT_IDENTIFIER = "transcript_id"
    REGION_TYPES = ["coding", "5PUTR", "3PUTR", "non coding", "intron"]
    
    def __init__(self, reference_gtf=None, **kwargs):
        ''' @param cfg Config object which contains paths to reference files - use DefaultConfig if not provided '''

        build = kwargs.get("build")
        cfg = kwargs.get("config")
        # these are used to group transcripts and genes
        self.transcript_id = kwargs.get("transcript_id", Reference.DEFAULT_TRANSCRIPT_IDENTIFIER)
        self.gene_id = kwargs.get("gene_id", Reference.DEFAULT_GENE_IDENTIFIER) 
        self.biotype = kwargs.get("biotype") # Fixed biotype option
        self.stranded = kwargs.get("stranded", True) # For regions
        self.standard_chromosomes_only = kwargs.get("standard_chromosomes_only", True)

        params = {arg for arg in (reference_gtf, build, cfg) if arg}
        if len(params) > 1:
            raise ValueError("You may only provide at most one of of %s" % params)

        if reference_gtf: # Old way
            cfg = util.Struct()
            cfg.reference_genes = reference_gtf
            cfg.reference_trna = kwargs.get("reference_trna")
        elif cfg:
            cfg = cfg
        else:
            if build:
                cfg = config.DefaultConfig(build)
            else:
                cfg = config.DefaultConfig()

        self.logger = logging.getLogger(__name__)
        self.logger.info("reference = %s", cfg.reference_genes)
        
        self.reference_genes = cfg.reference_genes
        self.reference_trna = cfg.reference_trna

        if not os.path.exists(self.reference_genes):
            message = "Could not load reference gene GTF '%s'" % self.reference_genes
            raise ValueError(message)

    def _load_transcripts_from_gtf(self, reference_genes_gtf):
        ''' Loads from gtf and sets biotype (from transcript_id) '''
        genomic_array_of_features = genomics.features_to_genomic_array_of_sets(HTSeq.GFF_Reader(reference_genes_gtf))
        if self.biotype:
            biotyper = gtf_to_genes_utils.HardcodedBioTyper(self.biotype)
        else:
            biotyper = gtf_to_genes_utils.TranscriptIdBioTyper(self.transcript_id)

        transcripts = {}
        for (_, features) in genomic_array_of_features.steps():
            for feature in features:
                if not feature.attr.get(Transcript.BIOTYPE):
                    biotyper.set_biotype(feature)
                transcript_id = feature.attr[self.transcript_id]
                transcript = transcripts.get(transcript_id)
                if not transcript:
                    transcript = Transcript(transcript_id, self)
                    transcripts[transcript_id] = transcript
                    
                transcript += feature
        return transcripts

    def _load_transcripts(self, gtf_filename):
        ''' Load transcripts from "gtf_filename" using gtf_to_genes if possible '''        
        transcripts = self._load_transcripts_from_gtf(gtf_filename)
        return transcripts


    @Lazy
    def transcripts(self):
        ''' dict of {"transcript_id" : Transcript} '''
        self.logger.info("loading transcripts")

        transcripts = self._load_transcripts(self.reference_genes)
        if self.standard_chromosomes_only:
            transcripts = {t_name:t for (t_name,t) in transcripts.iteritems() if genomics.format_chrom(t.iv.chrom, False) in HUMAN_CHROMOSOMES}
        self.regions = self.load_regions(transcripts)
        return transcripts

    @Lazy
    def _transcripts_for_gene(self):
        ''' dict of {"gene_name" : dict{"biotype" : transcript[]} } 
            This allows us to group transcripts easily, for genes and genes_by_biotype
        '''
        transcripts_for_gene_dict = defaultdict(lambda : defaultdict(list))
        for t in self.transcripts.values():
            gene_name = t.attr[self.gene_id]
            transcripts_for_gene_dict[gene_name][t.get_biotype()].append(t)
        return transcripts_for_gene_dict

    @Lazy
    def genes_by_biotype(self):
        ''' dict of {"biotype" : array_of_genes_biotype }
            This also includes 'tRNA' (from non-standard UCSC GTF) '''

        genes_by_biotype_dict = defaultdict(list)

        # Create a gene that represents all transcripts
        for (name, transcripts_per_biotype) in self._transcripts_for_gene.iteritems():
            for (biotype, transcripts) in transcripts_per_biotype.iteritems():
                gene = Gene(name, transcripts, biotype=biotype)
                genes_by_biotype_dict[biotype].append(gene)

        genes_by_biotype_dict[Reference.TRNA_BIOTYPE].extend(self.trna_genes.itervalues())

        return genes_by_biotype_dict

    @Lazy
    def trna_transcripts(self):
        ''' dict of {"tRNA_name" : Transcript}  '''
        return self._load_transcripts(self.reference_trna)

    @Lazy
    def trna_genes(self):
        ''' dict of {"tRNA_name" : Gene}  '''
        trna_genes = {}
        for transcript in self.trna_transcripts.itervalues():
            gene_name = transcript.get_gene_id()
            gene = Gene(gene_name, [transcript], biotype=Reference.TRNA_BIOTYPE)
            trna_genes[gene_name] = gene
        return trna_genes
    
    def load_regions(self, transcripts):
        regions = HTSeq.GenomicArray( "auto", stranded=self.stranded, typecode='O' )

        # Make everything that has a gene in it be "intron"
        for t in transcripts.values():
            regions[t.iv] = "intron"

        # Non-coding
        for t in transcripts.values():
            if not t.is_coding:
                for exon in t.features_by_type["exon"]:
                    regions[exon.iv] = "non coding"

        # Go through and add in gene specific details (overriding introns)
        for t in transcripts.values():
            if t.is_coding:
                # Add untranslated regions (using exons compared to cds_start, cds_end)
                (left, right) = ("5PUTR", "3PUTR")
                if t.iv.strand == '-': # Switch
                    (left, right) = (right, left)
                
                for exon in t.features_by_type["exon"]:
                    if exon.iv.start < t.cds_start:
                        end_non_coding = min(t.cds_start, exon.iv.end)
                        non_coding_interval = HTSeq.GenomicInterval(exon.iv.chrom, exon.iv.start, end_non_coding, exon.iv.strand)
                        regions[non_coding_interval] = left
                        t += HTSeq.GenomicFeature(left, left, non_coding_interval.copy())

                    if exon.iv.end > t.cds_end:
                        start_non_coding = max(t.cds_end, exon.iv.start)
                        non_coding_interval = HTSeq.GenomicInterval(exon.iv.chrom, start_non_coding, exon.iv.end, exon.iv.strand)
                        regions[non_coding_interval] = right
                        t += HTSeq.GenomicFeature(right, right, non_coding_interval.copy())
    
                for coding in t.features_by_type["CDS"]:
                    regions[coding.iv] = "coding"
                
        return regions

