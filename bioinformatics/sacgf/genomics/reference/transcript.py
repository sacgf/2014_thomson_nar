'''
A genetic transcript: ie a particular segment of DNA is copied into RNA by the enzyme RNA polymerase

Created in Reference from a .GTF
5/3/2013 by dlawrence: Removed get_protein() due to annotation/frame shift problems - If you want a protein, use a database
@see sacgf.genomics.reference.Reference

Created on Sep 6, 2012
@author: dlawrence
'''

from collections import defaultdict
import HTSeq

class Transcript(HTSeq.GenomicFeature):
    FEATURE_TYPE = "transcript"
    BIOTYPE = "gene_biotype"

    def __init__(self, transcript_id, reference):
        super(Transcript, self).__init__(transcript_id, Transcript.FEATURE_TYPE, None)
        self.reference = reference
        self.attr = { }
        self.features_by_type = defaultdict(set)

        # By default end of gene (ie empty)
        self.is_coding   = False
        self.cds_start   = None
        self.cds_end     = None


    def __iadd__(self, feature):
        self.features_by_type[feature.type].add(feature)
        if self.iv is None:
            self.iv = feature.iv.copy()
        else:
            self.iv.extend_to_include(feature.iv)
        if getattr(feature, "attr", None):
            self.attr.update(feature.attr)

        if feature.type in ["CDS", "start_codon", "stop_codon"]:
            self.is_coding = True
            # Continue down here, setting csd_start etc

            if self.cds_start is None:
                self.cds_start = feature.iv.start
            else:
                self.cds_start = min(self.cds_start, feature.iv.start)

            if self.cds_end is None:
                self.cds_end = feature.iv.end
            else:
                self.cds_end = max(self.cds_end, feature.iv.end)

        return self

    def get_id(self):
        return self.name
    
    def get_gene_id(self):
        return self.attr["gene_id"]

    def get_gene_name(self):
        return self.attr["gene_name"]

    def get_biotype(self):
        return self.attr[Transcript.BIOTYPE]

