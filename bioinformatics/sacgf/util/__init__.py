"""General Utilities (eg string/text processing etc)"""
from sacgf.util.file_utils import *
import locale
import sys
import os

class Struct:
    def __init__(self, **entries): self.__dict__.update(entries)

# From http://stackoverflow.com/questions/752308/split-array-into-smaller-arrays
def split_list(alist, wanted_parts=1):
    '''
    Split list into x parts, return a list of smaller lists
    Smaller lists will be same size if possible, or last list will have extras if necessary?
    '''
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts] #round down
             for i in range(wanted_parts) ]

def int_with_commas(n):
    ''' returns a string formatted with thousand separators '''
    # Requires python > 2.7
    # return "{:,}".format(value)
    locale.setlocale(locale.LC_ALL, '')
    return locale.format("%d", n, grouping=True)

def require_args(a, *args):
    if len(sys.argv) != len(args) + 2:
        args_str = " ".join([a] + list(args))
        sys.stderr.write("Usage %s %s \n" % (os.path.basename(sys.argv[0]), args_str)) 
        sys.exit(1)