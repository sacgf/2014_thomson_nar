import ConfigParser
import os

from sacgf.util.file_utils import base_project_dirname

def get_config_default(cfg, section, option, default=None):
    if cfg.has_option(section, option):
        return cfg.get(section, option)
    else:
        return default


class Config(object):
    def __init__(self, igenomes_dir, **kwargs):
        ''' 
            @param igenomes_dir: base directory containing iGenomes directory  
            @param defaults: overwrite_params: dict of dicts - {'section' : {'option' : value}}
            
            Loads: ../../config/reference.cfg (in source control)
        '''        
        defaults = {"igenomes.base.dir" : igenomes_dir}
        defaults.update(kwargs.get("defaults", {}))
        
        cfg = ConfigParser.SafeConfigParser(defaults)
        self.cfg = cfg

        # reference
        reference_cfg_filename = os.path.join(base_project_dirname(), "config/reference.cfg")
        self.cfg.readfp(open(reference_cfg_filename))
        
        overwrite_params = kwargs.get("overwrite_params", {})
        for (section, options_dict) in overwrite_params.iteritems():
            for (option, value) in options_dict.iteritems():
                cfg.set(section, option, value)
                
        self.check_mandatory({"reference" : "organism"}, "a project-specific configuration file or in the defaults parameter")
        self.check_mandatory({"reference" : "annotation_group"}, "UCSC or Ensembl")
        self.check_mandatory({"reference" : "build"}, "a project-specific configuration file or in the defaults parameter")
        
        self.reference_build = cfg.get("reference", "build")        
        self.reference_genes = cfg.get("reference", "genes")
        self.reference_genome = cfg.get("reference", "genome")
        self.reference_trna = cfg.get("reference", "trna")
        self.reference_mature_mirna_fasta = cfg.get("reference", "mature.mir.fasta")
        self.gtf_to_genes_index = cfg.get("reference", "gtf_to_genes_index")

    def check_mandatory(self, mandatory, expected_location):
        for (section, option) in mandatory.iteritems():
            if not self.cfg.has_option(section, option):
                raise ValueError("Missing required value for %s.%s. Generally you should set this in %s." % (section, option, expected_location))

class DefaultConfig(Config):
    def __init__(self, igenomes_dir, **kwargs):
        '''
        Key word arguments:
            organism (default=Homo_sapiens)
            annotation_group (default=UCSC)
            build (default=hg19)
        '''
        reference_params = {"organism" : kwargs.get("organism", "Homo_sapiens"),
                            "annotation_group" : kwargs.get("annotation_group", "UCSC"),
                            "build" : kwargs.get("build", "hg19")}
        super(DefaultConfig, self).__init__(igenomes_dir, overwrite_params={"reference" : reference_params})

class EnsemblConfig(Config):
    def __init__(self, igenomes_dir, **kwargs):
        '''
        Key word arguments:
            organism (default=Homo_sapiens)
            annotation_group (default=Ensembl)
            build (default=GRCh37)
        '''
        reference_params = {"organism" : kwargs.get("organism", "Homo_sapiens"),
                            "annotation_group" : "Ensembl",
                            "build" : kwargs.get("build", "GRCh37")}
        super(EnsemblConfig, self).__init__(igenomes_dir, overwrite_params={"reference" : reference_params})

