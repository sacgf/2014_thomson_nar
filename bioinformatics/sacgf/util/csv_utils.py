import csv
import logging
from sacgf.util import file_utils

def write_csv_dict(csv_file, headers, rows, extrasaction=None, dialect=None):
    '''
    default dialect = 'excel', other dialect option: 'excel-tab'
    These are the same optional arguments as csv.DictWriter
    headers=keys for dicts
    rows=list of dicts
    '''
    logger = logging.getLogger(__name__)
    
    if extrasaction is None:
        extrasaction = "raise"
    if dialect is None:
        dialect = 'excel'
    
    logger.info("Writing %s", csv_file)
    f = file_utils.file_or_file_name(csv_file, "wb")

    writer = csv.DictWriter(f, headers, extrasaction=extrasaction, dialect=dialect)
#        writer.writeheader()
#        e_r_s_a uses python 2.6
    writer.writerow(dict(zip(headers, headers)))
    writer.writerows(rows)

