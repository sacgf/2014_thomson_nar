import os

def file_or_file_name(f, mode='r'):
    if isinstance(f, basestring): # Works on unicode
        if 'w' in mode: # Create path if writing
            mk_path_for_file(f)
            
        return open(f, mode)
    elif isinstance(f, file):
        return f # Already a File object
    else:
        raise ValueError("'%s' (%s) not a file or string" % (f, type(f)))

def mk_path(path):
    if path and not os.path.exists(path):
        os.makedirs(path)

def mk_path_for_file(f):
    mk_path(os.path.dirname(f))

def name_from_file_name(file_name):
    '''Gets file name with removing extension and directory'''
    return os.path.splitext(os.path.basename(file_name))[0]

def file_to_hash(f, sep=None):
    data = {}
    for line in file_or_file_name(f):
        line = line.rstrip()
        key = line
        value = None
        if sep:
            items = line.split(sep)
            if len(items) >= 2:
                (key, value) = items[0:2]
        data[key] = value
    return data

def remove_gz_if_exists(filename):
    if filename.endswith(".gz"):
        filename = os.path.splitext(filename)[0] # remove .gz
    return filename

def base_project_dirname():
    ''' Returns the full path name of the project '''
    return up_dir(os.path.dirname(__file__), 2) # Relative to current file (change if you ever move!)

def up_dir(path, n):
    ''' Basically like os.path.join(path, "../" * n) but without the dots '''
    assert n >= 0
    path_components = path.split(os.path.sep)
    return os.path.sep.join(path_components[:-n])
