'''
Helpers for SciPy and numpy
 
Created on Jan 2, 2013
 
@author: dave
'''

import numpy as np

def safe_denominator(denominator):
    '''Function to help avoid 'division by zero' errors
    Returns np.NaN if denominator is zero, otherwise, returns denominator unchanged'''
    return denominator if denominator else np.NaN

