#!/bin/bash

IGENOMES_DIR=$1
CORES=4 #Script expects at least 2 cores. To use fewer, change it so that tasks do not launch to run in the background.
INPUT_FASTQ_SRNASEQ=MDA-MB-231_RNA-seq_sRNA.fastq.gz
INPUT_FASTQ_HC_GAII=MDA-MB-231_GAII_controls_HITS-CLIP_sRNA.fastq.gz
INPUT_FASTQ_HC_HiSeq=MDA-MB-231_HiSeq_controls_HITS-CLIP_sRNA.fastq.gz
BOWTIE_INDEX=${IGENOMES_DIR}/Homo_sapiens/UCSC/hg19/Sequence/Bowtie2Index/genome
TRNA_GFF=${IGENOMES_DIR}/Homo_sapiens/UCSC/hg19/Annotation/SmallRNA/tRNA_with_chr.hg19.gff
MIRNA_GFF=${IGENOMES_DIR}/Homo_sapiens/UCSC/hg19/Annotation/SmallRNA/miRNA.hg19.gff

echo "Convert GAII fastq encoding"
INPUT_FASTQ_HC=MDA-MB-231_controls_HITS-CLIP_sRNA.fastq.gz
fastq_to_sanger.py $INPUT_FASTQ_HC_GAII fastq-illumina
echo "Merge GAII and HiSeq fastq files"
zcat ${INPUT_FASTQ_HC_GAII%fastq.gz}sanger.fastq.gz $INPUT_FASTQ_HC_HiSeq | gzip > $INPUT_FASTQ_HC
rm ${INPUT_FASTQ_HC_GAII%fastq.gz}sanger.fastq.gz

echo "Trim adapters" #HiSeq for sRNA-seq, GAII for HC
cutadapt --quality-cutoff=28 --adapter=TGGAATTCTCGGGTGCCAAGGAACTCCAGTCAC --error-rate 0.2 --overlap=2 --minimum-length=18 --output=${INPUT_FASTQ_SRNASEQ%fastq.gz}trimmed.fastq.gz ${INPUT_FASTQ_SRNASEQ} &
cutadapt --quality-cutoff=28 --adapter=CAGACGACGAGCGGGATCGTATGCCGTCTTCTGCT --error-rate 0.2 --overlap=2 --minimum-length=18 --output=${INPUT_FASTQ_HC%fastq.gz}trimmed.fastq.gz ${INPUT_FASTQ_HC} &
wait

echo "Filter to remove low complexity sequences"
CLEANED_FASTQ_SRNASEQ=${INPUT_FASTQ_SRNASEQ%fastq.gz}cleaned.fastq.gz
CLEANED_FASTQ_HC=${INPUT_FASTQ_HC%fastq.gz}cleaned.fastq.gz
zcat ${INPUT_FASTQ_SRNASEQ%fastq.gz}trimmed.fastq.gz | perl $PRINSEQ_DIR/prinseq-lite.pl -fastq stdin -out_bad null -log -lc_method dust -lc_threshold 40 -out_good stdout | gzip > $CLEANED_FASTQ_SRNASEQ
zcat ${INPUT_FASTQ_HC%fastq.gz}trimmed.fastq.gz | perl $PRINSEQ_DIR/prinseq-lite.pl -fastq stdin -out_bad null -log -lc_method dust -lc_threshold 40 -out_good stdout | gzip > $CLEANED_FASTQ_HC

echo "Remove CCA from ends of reads"
fastq_remove_3p_seq.py --seq CCA --fastq $CLEANED_FASTQ_SRNASEQ &
fastq_remove_3p_seq.py --seq CCA --fastq $CLEANED_FASTQ_HC &
wait

echo "Map CCA-trimmed reads"
bowtie2 -p $CORES -t -q -N 0 -k 100 --end-to-end --sensitive -x $BOWTIE_INDEX -U ${CLEANED_FASTQ_SRNASEQ%.fastq.gz}_trimmed.fastq -S sRNA-seq_all-k100N0_noCCA.sam
bowtie2 -p $CORES -t -q -N 0 -k 100 --end-to-end --sensitive -x $BOWTIE_INDEX -U ${CLEANED_FASTQ_HC%.fastq.gz}_trimmed.fastq -S HITS-CLIP_sRNA_all-k100N0_noCCA.sam

samtools view -Sb sRNA-seq_all-k100N0_noCCA.sam > sRNA-seq_all-k100N0_noCCA.bam &
samtools view -Sb HITS-CLIP_sRNA_all-k100N0_noCCA.sam > HITS-CLIP_sRNA_all-k100N0_noCCA.bam &
wait
rm *noCCA.sam

echo "Get read IDs of reads that map to tRNAs and use these to get two fastq files: from file which contains reads that had CCA trimmed, get reads that map to tRNAs; from file that contains reads that did not have CCA to be trimmed, get reads that do not map to tRNAs."
FINAL_FASTQ_SRNA=${CLEANED_FASTQ_SRNASEQ%.fastq.gz}_tRNAs_trimmed.fastq
FINAL_FASTQ_HC=${CLEANED_FASTQ_HC%.fastq.gz}_tRNAs_trimmed.fastq
thomson_get_trna_bams_and_nontrna_reads.py --bam_no_cca sRNA-seq_all-k100N0_noCCA.bam --orig_fastq $CLEANED_FASTQ_SRNASEQ --no_cca_fastq ${CLEANED_FASTQ_SRNASEQ%.fastq.gz}_trimmed.fastq --output_fastq $FINAL_FASTQ_SRNA --min-size 17 2>thomson_get_trna_bams_and_nontrna_reads-sRNA.log --igenomes-dir $IGENOMES_DIR &
thomson_get_trna_bams_and_nontrna_reads.py --bam_no_cca HITS-CLIP_sRNA_all-k100N0_noCCA.bam --orig_fastq $CLEANED_FASTQ_HC --no_cca_fastq ${CLEANED_FASTQ_HC%.fastq.gz}_trimmed.fastq --output_fastq $FINAL_FASTQ_HC --min-size 17 2>thomson_get_trna_bams_and_nontrna_reads-HC.log --igenomes-dir $IGENOMES_DIR &
wait

echo "Map combined fastq files - this final run uses trimmed tRNA reads and bowtie 2 multimapping parameters"
FINAL_SAM_SRNA=sRNA-seq-k100N0_tRNAs_trimmed.sam
FINAL_SAM_HC=HITS-CLIP_sRNA-k100N0_tRNAs_trimmed.sam
bowtie2 -p $CORES -t -q -N 0 -k 100 --end-to-end --sensitive -x $BOWTIE_INDEX -U $FINAL_FASTQ_SRNA -S $FINAL_SAM_SRNA
bowtie2 -p $CORES -t -q -N 0 -k 100 --end-to-end --sensitive -x $BOWTIE_INDEX -U $FINAL_FASTQ_HC -S $FINAL_SAM_HC

echo "Convert sam to bam and clean"
for SAM in *sam; do samtools view -bS $SAM | samtools sort - ${SAM%.sam}; rm $SAM; done

echo "As a comparison, map with bowtie default parameters"
SAM_BOWTIE_DEFAULT_SRNA=sRNA-seq-default_tRNAs_trimmed.sam
SAM_BOWTIE_DEFAULT_HC=HITS-CLIP_sRNA-default_tRNAs_trimmed.sam
bowtie2 -p $CORES -t -q -x $BOWTIE_INDEX -U $FINAL_FASTQ_SRNA -S $SAM_BOWTIE_DEFAULT_SRNA
bowtie2 -p $CORES -t -q -x $BOWTIE_INDEX -U $FINAL_FASTQ_HC -S $SAM_BOWTIE_DEFAULT_HC

echo "Convert sam to bam and clean"
for SAM in *sam; do samtools view -bS $SAM | samtools sort - ${SAM%.sam}; rm $SAM; done

echo "As a comparison, map with BWA default parameters"
fastq_bwa_to_sam.sh $FINAL_FASTQ_SRNA $CORES
fastq_bwa_to_sam.sh $FINAL_FASTQ_HC $CORES

echo "Convert sam to bam and clean"
for SAM in *sam; do samtools view -bS $SAM | samtools sort - ${SAM%.sam}; rm $SAM; done

echo "Make multimapping info tables"
for BAM in *bam; do NAME=${BAM%.bam}; echo $NAME; thomson_multimapping_info_table.py --stranded -a $BAM -o ${NAME}_alignment_info.tsv --per-transcript-outfile ${NAME}_per_transcript_alignment_info.tsv --igenomes-dir $IGENOMES_DIR; done

echo "Make scatterplots to compare data and mapping methods"
if [ ! -e analyses ]; then
	mkdir analyses
fi

#Compare our pipeline vs same pipeline but mapped using default parameters
thomson_degree_of_multimapping_scatterplot.py -t1 sRNA-seq-default_tRNAs_trimmed_per_transcript_alignment_info.tsv -n1 'sRNA-default bowtie parameters' -t2 sRNA-seq-k100N0_tRNAs_trimmed_per_transcript_alignment_info.tsv -n2 'sRNA-multimapping bowtie parameters' -o analyses
thomson_degree_of_multimapping_scatterplot.py -t1 HITS-CLIP_sRNA-default_tRNAs_trimmed_per_transcript_alignment_info.tsv -n1 'HC-default bowtie parameters' -t2 HITS-CLIP_sRNA-k100N0_tRNAs_trimmed_per_transcript_alignment_info.tsv -n2 'HC-multimapping bowtie parameters' -o analyses

#Compare results using BWA vs bowtie default parameters
thomson_degree_of_multimapping_scatterplot.py -t1 sRNA-seq-default_tRNAs_trimmed_per_transcript_alignment_info.tsv -n1 'sRNA-default bowtie parameters' -t2 sRNA-seq.cleaned_tRNAs_trimmed.bwa_per_transcript_alignment_info.tsv -n2 'sRNA-default bwa parameters' -o analyses
thomson_degree_of_multimapping_scatterplot.py -t1 HITS-CLIP_sRNA-default_tRNAs_trimmed_per_transcript_alignment_info.tsv -n1 'HC-default bowtie parameters' -t2 HITS-CLIP_sRNA.cleaned_tRNAs_trimmed.bwa_per_transcript_alignment_info.tsv -n2 'HC-default bwa parameters' -o analyses

#Compare results from sRNAseq vs HITS-CLIP data
thomson_degree_of_multimapping_scatterplot.py -t1 HITS-CLIP_sRNA-k100N0_tRNAs_trimmed_per_transcript_alignment_info.tsv -n1 'HC-multimapping bowtie parameters' -t2 sRNA-seq-k100N0_tRNAs_trimmed_per_transcript_alignment_info.tsv -n2 'sRNA-multimapping bowtie parameters' -o analyses

echo "Produce read length histograms"
thomson_read_length_histogram.py -o analyses sRNA-seq-k100N0_tRNAs_trimmed_alignment_info.tsv
thomson_read_length_histogram.py -o analyses HITS-CLIP_sRNA-k100N0_tRNAs_trimmed_alignment_info.tsv

echo "Producing stats on where in genes reads originated"
thomson_biotype_multimapping_stats.py -a ${FINAL_SAM_SRNA%sam}bam -o analyses -b tRNA,snoRNA -l 40 --igenomes-dir $IGENOMES_DIR
thomson_biotype_multimapping_stats.py -a ${FINAL_SAM_HC%sam}bam -o analyses -b tRNA,snoRNA -l 40 --igenomes-dir $IGENOMES_DIR

echo "Calculate stats on 5' bias" 
thomson_5prime_bias.py -i sRNA-seq-k100N0_tRNAs_trimmed_alignment_info.tsv -o analyses -b miRNA,snoRNA
thomson_5prime_bias.py -i HITS-CLIP_sRNA-k100N0_tRNAs_trimmed_alignment_info.tsv -o analyses -b miRNA,snoRNA

echo "Make abundance line graph"
thomson_ncrna_gene_abundances_graph.py -i sRNA-seq-k100N0_tRNAs_trimmed_per_transcript_alignment_info.tsv -n 'sRNA-seq' -o analyses
thomson_ncrna_gene_abundances_graph.py -i HITS-CLIP_sRNA-k100N0_tRNAs_trimmed_per_transcript_alignment_info.tsv -n 'HITS-CLIP_sRNA' -o analyses

