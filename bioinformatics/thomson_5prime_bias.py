#!/usr/bin/env python
'''
Created on 24/09/2013
@author: Johnny
Take alignment_info files created from the thomson_pipeline script and calculate nucleotide bias at the 5' end of different biotypes. 
Uses weblogo to produce final pics: sudo pip install weblogo
list of biotypes: 3prime_overlapping_ncrna,antisense,IG_C_gene,IG_J_gene,IG_V_gene,IG_V_pseudogene,lincRNA,miRNA,misc_RNA,non_coding,
    polymorphic_pseudogene,processed_transcript,protein_coding,pseudogene,rRNA,sense_intronic,sense_overlapping,snoRNA,snRNA,TR_C_gene,
    TR_J_gene,tRNA,TR_V_gene,TR_V_pseudogene
'''
from argparse import ArgumentParser
import glob
from os import path
import os

import pandas as pd


def handle_args():
    parser = ArgumentParser(description='')
    parser.add_argument('-i', '--infile', required=True,  help="full path to tab separated alignment info file")
    parser.add_argument('-c', '--cutoff', default='1', type=int, help="cutoff value for number of identical reads allowed")
    parser.add_argument('-e', '--endPos', default='10', type=int, help="number of characters at 5' end to assess")
    parser.add_argument('-b', '--biotype', required=True, help='comma seperated list of biotype/s to analyse (no spaces - case sensitive)')
    parser.add_argument('-d', '--dinuc', action='store_true', help='add for dinucleotide analysis')
    parser.add_argument('-o', '--output', required=True, help='output dir')
    return parser.parse_args()

args = handle_args()

### Used to set summary print of dataframes
pd.set_option("display.large_repr", "info")
output_df = pd.DataFrame()
dinucAnalysis = ""
output_file = path.join(args.output, "%s_%s%s_nucBiasCounts.transfac")

### Reads in tsv file
print "reading in alignment info file: " + args.infile
align_info_df = pd.read_csv(args.infile, sep='\t', header=0, index_col=False)

for biotype in args.biotype.split(","):
    print "analysing biotype: " + biotype
    
    ### Clean the dataframe and  filter
    align_info_cutoff_filtered_df = align_info_df[(align_info_df["Number_of_identical_reads"] >= args.cutoff)] 
    align_info_biotype_filtered_df = align_info_cutoff_filtered_df[(align_info_cutoff_filtered_df["Gene_biotypes"] == biotype)]
    
    ### Collect some stats to print to stdout
    row_count = len(align_info_biotype_filtered_df)
    print "total # of features at cutoff: " + str(row_count)
    
    ### Check to see if there is enough features to process weblogos
    if row_count > 25:
        if args.dinuc:
            startPos = jumpback = 2
            dinucAnalysis = "_di"
        else:
            startPos = jumpback = 1
        
        for i in range(startPos, args.endPos + 1):
            char_df = align_info_biotype_filtered_df["Sequence"].str[i-jumpback:i]
            char_df = char_df.replace("T", "U")
            output_df["0" + str(i)] = char_df.value_counts()
        
        if 'N' in output_df.columns:
            transfac_df = output_df.drop('N').transpose()
            transfac_df = transfac_df.fillna(0)
        else:
            transfac_df = output_df.transpose()
            transfac_df = transfac_df.fillna(0)
        transfac_df.to_csv(output_file % (os.path.splitext(args.infile)[0], biotype, dinucAnalysis), sep="\t", index_label="PO")
    else:
        print '<25 features! Not enough to run weblogo' 

### Create weblogos for those biotypes where a transfac file was created.   
if not args.dinuc:
    print "creating sequence logos"
    transfac_file_names = glob.glob(path.join(args.output, "*.transfac"))
    transfac_file_names_str = " ".join(transfac_file_names)
    os.system("for f in " + transfac_file_names_str + "; do weblogo -f $f -D transfac -o ${f%.*}.png -F png_print -s large -S 1 -c classic --errorbars NO; done")

