#!/usr/bin/env python
'''
Created on 04/04/2014
@author: katherine (hacked by Johnny)
'''
import HTSeq
from argparse import ArgumentParser
from collections import Counter
from os import path

import pandas as pd
from sacgf import genomics
from sacgf.genomics import genomic_utils
from sacgf.genomics.reference.reference import Reference
from sacgf.util import file_utils
from sacgf.util.config import EnsemblConfig, DefaultConfig


def handle_args():
    parser = ArgumentParser(description='Count % of reads in regions of ensemble biotype(s)')
    parser.add_argument('-a', '--aln-file', required=True, help='full path to bam file')
    parser.add_argument('-o', '--outdir', required=True, help='Output directory')
    parser.add_argument('-b', '--biotype', required=True, help='comma separated list of biotype/s to analyse (no spaces - case sensitive).')
    parser.add_argument('-l', '--read_length_cutoff', default='50', type=int, help='maximum allowed length of reads to analyse (default=50)')
    parser.add_argument('-t', '--header_trailer_length', default='40', type=int, help='maximum allowed length of header and trailer sequence (specifically for tRNAs, default=40)')
    parser.add_argument("--igenomes-dir", required=True, help="Directory of Illumina iGenomes (contains 'Homo_sapiens/UCSC/hg19')")
    return parser.parse_args()

def assess_biotype(biotype, biotype_list, out_file_name, read_cutoff, header_trailer_length_cutoff, wiggle):
    print "assessing biotype: " + biotype
    biotype_gas = HTSeq.GenomicArrayOfSets(chroms="auto", stranded=True)
    biotype_5p_ivs = {}
    biotype_3p_ivs = {}
    biotype_header_ivs = {}
    biotype_trailer_ivs = {}
    
    for entry in biotype_list:
        for transcript in entry.transcripts:
            transcript.iv.chrom = genomics.format_chrom(transcript.iv.chrom, want_chr)
            biotype_gas[transcript.iv] += transcript

            half_length = transcript.iv.length / 2
            iv_5p = genomics.iv_from_pos_and_offsets(transcript.iv.start_d_as_pos, half_length, 0)
            remaining_length = transcript.iv.length - half_length
            iv_3p = genomics.iv_from_pos_and_offsets(iv_5p.end_d_as_pos, remaining_length, 0)
            if transcript.iv.strand == "+":
                iv_header = HTSeq.GenomicInterval( transcript.iv.chrom, transcript.iv.start_d - header_trailer_length_cutoff, transcript.iv.start_d + wiggle, transcript.iv.strand )
                iv_trailer = HTSeq.GenomicInterval( transcript.iv.chrom, transcript.iv.end_d - wiggle, transcript.iv.end_d + header_trailer_length_cutoff, transcript.iv.strand )
            else:
                iv_header = HTSeq.GenomicInterval( transcript.iv.chrom, transcript.iv.start_d - wiggle, transcript.iv.start_d + header_trailer_length_cutoff, transcript.iv.strand )
                iv_trailer = HTSeq.GenomicInterval( transcript.iv.chrom, transcript.iv.end_d - header_trailer_length_cutoff, transcript.iv.end_d + wiggle, transcript.iv.strand )            

            biotype_5p_ivs[transcript.iv] = iv_5p
            biotype_3p_ivs[transcript.iv] = iv_3p
            biotype_header_ivs[transcript.iv] = iv_header
            biotype_trailer_ivs[transcript.iv] = iv_trailer
    
    biotype_5p_counts = Counter()
    biotype_3p_counts = Counter()
    biotype_middle_counts = Counter()
    biotype_header_counts = Counter()
    biotype_trailer_counts = Counter()
    biotype_other_counts = Counter()
    biotype_analysed_read_names = set()
    for aln in alignment_reader:
           
        if len(aln.read) < read_cutoff: #Skip all reads greater than cutoff in length
            if aln.read.name in biotype_analysed_read_names: #Skip all reads which were already assigned
                continue
            # catch unaligned reads and count them
            try:
                biotype_transcripts = genomics.get_unique_features_from_genomic_array_of_sets_iv(biotype_gas, aln.iv)
            except KeyError:
                pass

            if len(biotype_transcripts) > 1:
                print "Weird, found >1 %s overlapping one read" % biotype

            #Split biotype in half
            elif len(biotype_transcripts) == 1:
                biotype_transcript = biotype_transcripts.pop()
                biotype_5p_iv = biotype_5p_ivs[biotype_transcript.iv]
                biotype_3p_iv = biotype_3p_ivs[biotype_transcript.iv]
                biotype_header_iv = biotype_header_ivs[biotype_transcript.iv]
                biotype_trailer_iv = biotype_trailer_ivs[biotype_transcript.iv]
                biotype_whole_iv = biotype_transcript.iv
                if biotype == 'tRNA':
                    gene_name = biotype_transcript.get_gene_id() #tRNAs don't have a 'gene name'
                else:
                    gene_name = biotype_transcript.get_gene_name()
                #Does it fit entirely into 5'?  
                if aln.iv.is_contained_in(biotype_5p_iv):
                    biotype_5p_counts[gene_name] += 1 

                #Does it fit entirely into 3'? 
                elif aln.iv.is_contained_in(biotype_3p_iv):
                    biotype_3p_counts[gene_name] += 1

                #if it is entirely inside the transcript but not in either group above, assign it to middle group.
                elif aln.iv.is_contained_in(biotype_whole_iv): 
                    biotype_middle_counts[gene_name] += 1

                elif aln.iv.is_contained_in(biotype_header_iv): 
                    biotype_header_counts[gene_name] += 1

                elif aln.iv.is_contained_in(biotype_trailer_iv): 
                    biotype_trailer_counts[gene_name] += 1

                else: #Everything else, these are the reads that fall off 5' and/or 3' end of transcript
                    biotype_other_counts[gene_name] += 1

                #If a read was mapped, only want to consider it the first time.
                biotype_analysed_read_names.add(aln.read.name)
    
    df = pd.DataFrame({biotype + '_5p': biotype_5p_counts, 
                       biotype + '_3p':biotype_3p_counts, 
                       biotype + '_middle':biotype_middle_counts, 
                       biotype + '_header':biotype_header_counts, 
                       biotype + '_trailer':biotype_trailer_counts, 
                       biotype + '_other':biotype_other_counts})
    df.to_csv(out_file_name, sep='\t')

    total_5p_count = sum(biotype_5p_counts.values())
    total_3p_count = sum(biotype_3p_counts.values())
    total_middle_count = sum(biotype_middle_counts.values())
    total_header_count = sum(biotype_header_counts.values())
    total_trailer_count = sum(biotype_trailer_counts.values())
    total_remaining_count = sum(biotype_other_counts.values())

    print "There are \n\t%d reads in 5'\n\t%d reads in 3'\n\t%d reads in middle\n\t%d reads in header\n\t%d reads in trailer\n\t%d reads left over\n\t\n" \
        % (total_5p_count, total_3p_count, total_middle_count, total_header_count, total_trailer_count, total_remaining_count)


args = handle_args()

#Parameters
alignment_file = args.aln_file
outdir = args.outdir
want_chr = True #whether to have 'chr' in front of chromosome name
igenomes_dir = args.igenomes_dir
biotypes_str = args.biotype
wiggle = 5

#Setup
alignment_reader = genomic_utils.load_alignment_reader(alignment_file, want_chr)
ensembl_reference = Reference(config=EnsemblConfig(igenomes_dir))
reference = Reference(config=DefaultConfig(igenomes_dir))
sample_name = file_utils.name_from_file_name(alignment_file)

biotypes = biotypes_str.split(",")

for biotype in biotypes:
    output_counts_file = path.join(outdir, '%s_%s_counts.tsv' % (sample_name, biotype))
    
    if biotype == "tRNA":
        print 'setting up trna data...' 
        trna_list = ensembl_reference.trna_genes.values()
        assess_biotype(biotype, trna_list, output_counts_file, args.read_length_cutoff, args.header_trailer_length, wiggle)
    else:
        print "setting up " + biotype + " data..."
        biotype_list = ensembl_reference.genes_by_biotype[biotype]
        assess_biotype(biotype, biotype_list, output_counts_file, args.read_length_cutoff, args.header_trailer_length, wiggle)

