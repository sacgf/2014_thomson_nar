#!/usr/bin/env python
'''
Created on 19/06/2013
@author: katherine

Graph to investigate the degree to which each class of small RNA multimaps.
Produces 2 graphs - one with only the highest-expressed transcript per gene plotted
One with every transcript plotted independently.
These approaches are necessary because the input data allows multimapping so transcript counts should not be summed. 
'''
from argparse import ArgumentParser
from os import path

import matplotlib.pyplot as pl
import numpy as np
import pandas as pd

def handle_args():
    parser = ArgumentParser(description='')
    parser.add_argument('-t1', '--table1',  type=str, required=True,  help='path to per-transcript alignment counts table for sample 1')
    parser.add_argument('-t2', '--table2',  type=str, required=True,  help='path to per-transcript alignment counts table for sample 2')
    parser.add_argument('-n1', '--name1',  type=str, required=True,  help='name of sample 1')
    parser.add_argument('-n2', '--name2',  type=str, required=True,  help='name of sample 2')
    parser.add_argument('-o', '--outdir',  type=str, required=True,  help='Output directory')
    parser.add_argument('--no-legend', type=bool, required=False, default=True, help='Remove legend from graph')
    parser.add_argument('--legend-loc',  type=int, default=4, help='Matplotlib legend loc code (int)')
    return parser.parse_args()

args = handle_args()

#Parameters
outdir = args.outdir
table1 = args.table1
table2 = args.table2
name1 = args.name1
name2 = args.name2
no_legend = args.no_legend
legend_loc = args.legend_loc
nc_rna_biotypes = {'miRNA':'r', "snoRNA":'b', "rRNA":'g', 'snRNA':'y', 'tRNA':'0.4', "yRNA":'c'}
yrnas = ['RNY1', 'RNY3', 'RNY4', 'RNY5']
all_transcripts_graph_file = path.join(outdir, "%s_vs_%s-all_transcripts_scatterplot.png" % (name1, name2))
one_transcript_graph_file = path.join(outdir, "%s_vs_%s-1_transcript_per_gene_scatterplot.png" % (name1, name2))

#Setup
pd.set_option("display.large_repr", "info")   
sample1_df = pd.read_table(table1, sep='\t', index_col=0)
sample2_df = pd.read_table(table2, sep='\t', index_col=0)

#Prepare data
all_df = sample1_df.copy()
all_df.rename(columns={'alignment_counts':name1}, inplace=True)
all_df['%s' % name2] = sample2_df['alignment_counts']

#Identify yRNAs
yrnas_mask = all_df['gene_name'].isin(yrnas)
all_df.loc[yrnas_mask, 'biotype'] = 'yRNA'

all_df = all_df[all_df['biotype'].isin(nc_rna_biotypes.keys())]    
all_df.to_csv(path.join(outdir, "%s_vs_%s-all_transcripts_scatterplot_data.txt" % (name1, name2)), sep='\t')

#Plot all transcripts
pl.figure(figsize=[6.5, 6.5])
for nc_biotype, color in nc_rna_biotypes.iteritems():
    num_genes = len(all_df[all_df['biotype'] == nc_biotype])
    x = np.log2(all_df[name1][all_df['biotype'] == nc_biotype])
    y = np.log2(all_df[name2][all_df['biotype'] == nc_biotype])
    
    pl.scatter(x, y, color=color, marker='o', alpha=0.5, label="%s (n=%d)" % (nc_biotype, num_genes))

pl.xlabel('Log2 number of reads (%s)' % name1)
pl.ylabel('Log2 number of reads (%s)' % name2)
if not no_legend:
    pl.legend(loc=legend_loc)
pl.xlim(xmin = -1, xmax=22)
pl.ylim(ymin = -1, ymax=22)

pl.savefig(all_transcripts_graph_file)
pl.close()

#Plot one transcript per gene - one with highest expression
transcript_per_gene = all_df.groupby('gene_name')
sample1_gene_data = transcript_per_gene[name1].max()
sample2_gene_data = transcript_per_gene[name2].max()
gene_biotypes = transcript_per_gene['biotype'].first()

one_transcript_df = pd.concat([sample1_gene_data, sample2_gene_data, gene_biotypes], axis=1)
one_transcript_df.to_csv(path.join(outdir, "%s_vs_%s-1_transcript_per_gene_scatterplot_data.txt" % (name1, name2)), sep='\t')

pl.figure(figsize=[6.5, 6.5])
for nc_biotype, color in nc_rna_biotypes.iteritems():
    biotype_mask = one_transcript_df['biotype'] == nc_biotype
    x = np.log2(one_transcript_df[name1][biotype_mask])
    y = np.log2(one_transcript_df[name2][biotype_mask])
    num_genes = len(x)
    
    pl.scatter(x, y, color=color, marker='o', alpha=0.5, label="%s (n=%d)" % (nc_biotype, num_genes))

pl.xlabel('Log2 number of reads (%s)' % name1)
pl.ylabel('Log2 number of reads (%s)' % name2)
if not no_legend:
    pl.legend(loc=legend_loc)
pl.xlim(xmin = -1, xmax=22)
pl.ylim(ymin = -1, ymax=22)

pl.savefig(one_transcript_graph_file)
pl.close()


