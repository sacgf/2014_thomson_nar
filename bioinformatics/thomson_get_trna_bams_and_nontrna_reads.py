#!/usr/bin/env python
'''
Created on 12/06/2013
@author: katherine
'''

import HTSeq
from argparse import ArgumentParser
from os import path
import sys

from sacgf.genomics.reference.reference import Reference
from sacgf.util import int_with_commas
from sacgf.util.config import DefaultConfig
from sacgf.util.file_utils import name_from_file_name


def handle_args():    
    parser = ArgumentParser(description='Prepares a fastq of tRNA reads with 3\' CCA trimmed and all other reads untrimmed')
    parser.add_argument('--bam_no_cca', type=str,   required=True,  help='BAM alignment file from reads with CCA trimmed off')
    parser.add_argument('--orig_fastq', type=str,   required=True, help='the fastq file used to trim CCA')
    parser.add_argument('--no_cca_fastq', type=str,   required=True,  help='Fastq file of reads with CCA trimmed off')
    parser.add_argument('--output_fastq', type=str, required=True, help='Fastq file to print to')
    parser.add_argument('--min-size', type=int, required=False, default=0, help='Minimum size (bp) of read to keep, default=0 (keep all)')
    parser.add_argument("--igenomes-dir", required=True, help="Directory of Illumina iGenomes (contains 'Homo_sapiens/UCSC/hg19')")
    return parser.parse_args()

args = handle_args()

#Parameters
bam_no_cca_file = args.bam_no_cca
whole_original_fastq_file = args.orig_fastq
no_cca_fastq_file = args.no_cca_fastq
output_fastq_path = args.output_fastq
min_read_length = args.min_size
igenomes_dir = args.igenomes_dir

#Setup
reference = Reference(config=DefaultConfig(igenomes_dir))
        
whole_original_fastq_name = name_from_file_name(whole_original_fastq_file)
sys.stderr.write("Input files: %s, %s\n" % (whole_original_fastq_file, bam_no_cca_file))
sys.stderr.write("Output files: %s\n" % (output_fastq_path))

#Don't overwrite existing files
if path.exists(output_fastq_path):
    sys.exit("Error: Output file exists, please delete/move/rename it: %s\n" % output_fastq_path)

    
sys.stderr.write("Stage 1: Get names of reads that aligned to tRNAs\n")

no_cca_bam_reader = HTSeq.BAM_Reader(bam_no_cca_file)

trnas_genomic_array = HTSeq.GenomicArray('auto', stranded=True, typecode='b', storage='step')
for _, trna in reference.trna_genes.iteritems():
    trnas_genomic_array[trna.iv] = True
    
#Get unique read names of all alignments which overlap tRNA features from without-CCA dataset.
trna_read_names_with_nocca = set()
a = 0
for i, alignment in enumerate(no_cca_bam_reader):
    if alignment.aligned:
        for _, value in trnas_genomic_array[alignment.iv].steps():
            if value: #tRNAs = true
                trna_read_names_with_nocca.add(alignment.read.name)
                a += 1
                continue #only examine each alignment once...
          
    if not (i + 1) % 1000000: 
        sys.stderr.write("Read %d alignments\n" % (int(i) + 1))
sys.stderr.write("Examined %s alignments, found %s tRNA alignments (%.2f %%) from %s reads\n" % (int_with_commas(i), int_with_commas(a), ((float(a) / i) *100), int_with_commas(len(trna_read_names_with_nocca))))


sys.stderr.write("Stage 2: Get trimmed reads which did map to tRNAs and untrimmed reads for which the trimmed version didn't map to tRNAs\n")

no_cca_fastq_reader = HTSeq.FastqReader(no_cca_fastq_file)
output_fastq_file = open(output_fastq_path, 'w')
count_of_reads_written_to_file = 0
count_reads_too_short = 0
for b, read in enumerate(no_cca_fastq_reader):
    
    if read.name in trna_read_names_with_nocca: #is CCA-trimmed, tRNA read
        #Filter to remove short reads
        if len(read.seq) < min_read_length:
            count_reads_too_short += 1
        
        else:       
            read.write_to_fastq_file(output_fastq_file)
            count_of_reads_written_to_file += 1
        
sys.stderr.write("Examined %s -CCA reads: %s are tRNA reads, %s discarded because too short (<%dbp)\n" % (int_with_commas(b + 1), int_with_commas(count_of_reads_written_to_file), int_with_commas(count_reads_too_short), min_read_length))

#All other reads should not be trimmed.
#Therefore add the non-trimmed version of all the reads which were not written above to the output file
orig_fastq_reader = HTSeq.FastqReader(whole_original_fastq_file)
c = 0
for d, read in enumerate(orig_fastq_reader):
    
    if not (read.name in trna_read_names_with_nocca) : #is not CCA-trimmed, tRNA read
        c += 1
        
        if len(read.seq) < min_read_length:
            count_reads_too_short += 1
        
        else:
            read.write_to_fastq_file(output_fastq_file)            
            count_of_reads_written_to_file += 1
    
output_fastq_file.close()

sys.stderr.write("Examined %s reads: %s are non-trimmed or non-tRNA reads, %s discarded because too short (<%dbp)\n" % (int_with_commas(d + 1), int_with_commas(c), int_with_commas(count_reads_too_short), min_read_length))        
sys.stderr.write("Finished, %s reads total printed to file\n" % (int_with_commas(count_of_reads_written_to_file)))

