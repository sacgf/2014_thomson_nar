#!/usr/bin/env python
'''
Created on 04/03/2013
@author: katherine
'''
import HTSeq
from argparse import ArgumentParser
from collections import Counter, defaultdict
import csv
import sys

import pandas as pd
from sacgf import genomics, util
from sacgf.genomics import genomic_utils
from sacgf.genomics.reference.reference import Reference
from sacgf.util.config import EnsemblConfig, DefaultConfig
from sacgf.util.stats_utils import safe_denominator

def handle_args():
    parser = ArgumentParser(description='Annotate each alignment in bam/sam with names of overlapping features.')
    parser.add_argument('-a', '--aln-file',  required=True,  help='full path to bam file')
    parser.add_argument('-o', '--outfile',  required=True,  help='output file')
    parser.add_argument('--per-transcript-outfile',  required=True,  help='output file for per-gene alignment counts')
    parser.add_argument('--stranded',  action='store_true', help='Alignment file is strand-specific')
    parser.add_argument('--organism', type=str, default='Homo_sapiens', help='Reference Genome organism (in iGenomes). Default: Homo_sapiens')
    parser.add_argument("--igenomes-dir", required=True, help="Directory of Illumina iGenomes (contains 'Homo_sapiens/UCSC/hg19')")
    return parser.parse_args()

args = handle_args()

#Parameters
want_chr = True #whether to have 'chr' in front of chromosome name
alignment_file = args.aln_file
stranded = args.stranded
outfile = args.outfile
per_transcript_outfile = args.per_transcript_outfile
igenomes_dir = args.igenomes_dir

#Setup
reference = Reference(config=DefaultConfig(igenomes_dir))
ensembl_reference = Reference(config=EnsemblConfig(igenomes_dir))
trna_gtf = reference.reference_trna
ensembl_gtf  = ensembl_reference.reference_genes

alignment_reader = genomic_utils.load_alignment_reader(alignment_file, want_chr)

if stranded:
    sys.stderr.write("Treating alignments as stranded\n")
else:
    sys.stderr.write("Treating alignments as unstranded\n")

sys.stderr.write("Loading genes into genomic array of sets... ")
#Load all intervals in ensembl and tRNA genes into array of sets
regions = HTSeq.GenomicArrayOfSets("auto", stranded=stranded, storage='step')
for feature in HTSeq.GFF_Reader(ensembl_gtf):
    feature.iv.chrom = genomics.format_chrom(feature.iv.chrom, want_chr)
    feature.name = feature.attr['gene_name']
    regions[feature.iv] += feature #set contains list of gene name, biotype
sys.stderr.write('finished genes, starting tRNAs...')

for feature in HTSeq.GFF_Reader(trna_gtf):
    feature.iv.chrom = genomics.format_chrom(feature.iv.chrom, want_chr)
    feature.attr['gene_biotype'] = 'tRNA'
    feature.attr['gene_name'] = feature.attr['gene_id'] #This is unique per transcript
    regions[feature.iv] += feature
sys.stderr.write("done\n")

#Read alignments and extract info
alignment_counts = Counter()
alignment_counts_per_transcript = Counter()        
biotypes_per_transcript = {}
gene_names_per_transcript = {}
locations = defaultdict(set)
gene_names_per_seq = defaultdict(set)
gene_biotype_per_seq = defaultdict(set)
read_names_per_seq = defaultdict(set)
iv_gene_names = list()
a = 0
for (i, aln) in enumerate(alignment_reader):
    seq = aln.read.seq
    
    read_names_per_seq[seq].add(aln.read.name) #get unique set of IDs of reads with same seq - to count # duplicate reads
    
    if aln.aligned: #reads that were successfully aligned
        alignment_counts[seq] += 1 #count number of alignments
        
        iv_as_string = genomics.iv_format(aln.iv)
        locations[seq].add(iv_as_string) #unique set of genomic intervals      
        
        #get gene names from gtfs
        for (_, features) in regions[aln.iv].steps():
            for feature in features:
                gene_biotype_per_seq[seq].add(feature.attr['gene_biotype'])
                gene_names_per_seq[seq].add(feature.name)
                
                #Per-transcript data
                alignment_counts_per_transcript[feature.attr['gene_id']] += 1
                biotypes_per_transcript[feature.attr['gene_id']] = feature.attr['gene_biotype']
                gene_names_per_transcript[feature.attr['gene_id']] = feature.attr['gene_name']
        a += 1

    if not ((i + 1) % 1000000):
        sys.stderr.write("Processed %s alignments\n" % util.int_with_commas(i + 1))
        
sys.stderr.write('%d completed alignments (unaligned + aligned): %d successful alignments + %d unaligned reads\n' % (i + 1, a, i + 1 - a))

#Output per-transcript data
data = pd.DataFrame(data={'biotype': biotypes_per_transcript, 'gene_name': gene_names_per_transcript, 'alignment_counts': alignment_counts_per_transcript})
print "Printing per transcript data to file %s" % per_transcript_outfile
data.to_csv(per_transcript_outfile, sep='\t')

read_counts = {seq : len(read_names) for (seq, read_names) in read_names_per_seq.iteritems()}

sys.stderr.write("Printing table of all reads %s\n" % outfile)
header = ["Sequence", "Length(bp)", "Number_of_identical_reads", "Total_number_of_alignments", "Normalisation_factor", "Number_of_unique_alignments", "Gene_names", "Gene_biotypes", "Genomic_locations"]
with open(outfile, "wb") as f:
    writer = csv.DictWriter(f, header, delimiter='\t')
    writer.writeheader()
    
    #Sort output table so that the sequences with the most duplicates are at the top
    for (seq, num_identical_reads) in sorted(read_counts.iteritems(), key=lambda i : i[1], reverse=True):
       
        num_alignments = alignment_counts.get(seq, 0) #Total
        num_unique_alignments = len(locations.get(seq, "")) #Number of unique alignments = number of unique genomic locations
        #This is fraction of a real read each alignment represents. Sum of these values for all alignments should = Total # of reads
        normalisation_factor = float(num_identical_reads) / float(safe_denominator(num_alignments)) 
        
        gene_names = ", ".join(gene_names_per_seq.get(seq, ""))
        gene_biotypes = ", ".join(gene_biotype_per_seq.get(seq, ""))
        
        genomic_locations = ", ".join(locations.get(seq, "")) #get string of genomic locations
        
        data = {"Sequence": seq, "Length(bp)": len(seq) , 
                "Number_of_identical_reads": num_identical_reads, "Total_number_of_alignments": num_alignments, 
                "Normalisation_factor": normalisation_factor, "Number_of_unique_alignments": num_unique_alignments, 
                "Gene_names": gene_names, "Gene_biotypes": gene_biotypes, "Genomic_locations": genomic_locations}
        writer.writerow(data)
        
