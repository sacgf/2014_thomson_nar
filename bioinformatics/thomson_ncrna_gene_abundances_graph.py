#!/usr/bin/env python
'''
Created on 18/03/2013
@author: katherine

Use one locus/transcript per gene - the most highly expressed locus (can't combine all the transcripts
because some genes, e.g. snRNAs, have multiple loci which would result in counting some reads twice).
'''

from argparse import ArgumentParser
from os import path

import matplotlib.pyplot as pl
from sacgf import util
from sacgf.util import file_utils
import pandas as pd

def handle_args():
    parser = ArgumentParser(description='Annotate each alignment in bam/sam with names of overlapping features.')
    parser.add_argument('-i', '--input-table',  required=True,  help='Path to table of read counts per transcript')
    parser.add_argument('-n', '--name',  type=str, required=False,  help='Experiment name')
    parser.add_argument('-o', '--outdir',  type=str, default=".",  help='Output directory, default: current dir')
    return parser.parse_args()

args = handle_args()

#Parameters
xmax = 100 #how many genes to show
nc_rna_biotypes = set(['miRNA', 'snRNA', 'tRNA', "misc_RNA", "rRNA", "snoRNA"])
transcript_counts_file = args.input_table
outdir = args.outdir
colors = {'miRNA': 'r', 'snoRNA': 'b', 'rRNA': 'g', 'snRNA': 'y', 'yRNA': 'c', 'tRNA': '0.4'}
interesting_biotypes = colors.keys()
yrnas = ['RNY1', 'RNY3', 'RNY4', 'RNY5']

if args.name:
    expt_name = args.name
else:
    expt_name = file_utils.name_from_file_name(transcript_counts_file)
graph_image = path.join(outdir, "Abundance_linegraph-%s.png" % expt_name)
graph_data_file = path.join(outdir, "Abundance_linegraph_data-%s.tsv" % expt_name)

#Setup
df = pd.read_table(transcript_counts_file, index_col=0)

#set yRNA biotype
yrnas_mask = df['gene_name'].isin(yrnas)
df.loc[yrnas_mask, 'biotype'] = 'yRNA'

#Cut down
df = df[df['biotype'].isin(interesting_biotypes)]

#Plot per biotype
biotypes = set(df['biotype'].values)

x_label = "Non-coding RNA genes"
y_label = "Transcript abundance (number of reads)"
fig = pl.figure()
pl.xlabel(x_label)
pl.ylabel(y_label)

data = []
for biotype in biotypes:
    biotype_df = df[df['biotype'] == biotype]
    
    #Get counts for most abundant transcript per gene
    transcript_per_gene = biotype_df.groupby('gene_name')
    gene_data = transcript_per_gene['alignment_counts'].max()
    
    gene_data.sort(ascending=False)
    
    color = colors.get(biotype, "k")
    pl.plot(gene_data, color=color, linestyle='-', label=biotype[:10])
    
    #Prepare data to print to file
    gene_data = pd.DataFrame(gene_data)
    gene_data['biotype'] = biotype
    data.append(gene_data)
    
pl.xlim(xmin=0, xmax=xmax)
pl.legend(loc='upper right', ncol=3, prop={'size':12})

util.mk_path_for_file(graph_image)
pl.savefig(graph_image)
pl.close()
    
#Graph data to file
graph_data_df = pd.concat(data)
graph_data_df.to_csv(graph_data_file, sep='\t')

