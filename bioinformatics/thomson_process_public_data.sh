#!/bin/bash -l

#This script processes public IP data using the same pipeline as was used for our data.
#Input: iGenomes directory and one single-end gzipped fastq.
#Final Output: read length histograms for the public data

IGENOMES_DIR=$1
FASTQ_IN=$2
ADAPTER=$3
CORES=4
BOWTIE_INDEX=${IGENOMES_DIR}/Homo_sapiens/UCSC/hg19/Sequence/Bowtie2Index/genome

echo "Trim adapters" #HiSeq
#cutadapt --quality-cutoff=28 --adapter=$ADAPTER --error-rate 0.2 --overlap=2 --minimum-length=18 --output=${FASTQ_IN%fastq.gz}trimmed.fastq.gz $FASTQ_IN

echo "Filter to remove low complexity sequences"
CLEANED_FASTQ=${FASTQ_IN%fastq.gz}cleaned.fastq.gz
#zcat ${FASTQ_IN%fastq.gz}trimmed.fastq.gz | perl $PRINSEQ_DIR/prinseq-lite.pl -fastq stdin -out_bad null -lc_method dust -lc_threshold 40 -out_good stdout | gzip > $CLEANED_FASTQ

echo "Remove CCA from ends of reads"
fastq_remove_3p_seq.py --seq CCA --fastq $CLEANED_FASTQ

echo "Map CCA-trimmed reads"
NAME=`basename ${FASTQ_IN%.fastq.gz}`
NOCCA_SAM=${NAME}-k100N0_noCCA.sam
NOCCA_BAM=${NOCCA_SAM%sam}bam
bowtie2 -p $CORES -t -q -N 0 -k 100 --end-to-end --sensitive -x $BOWTIE_INDEX -U ${CLEANED_FASTQ%.fastq.gz}_trimmed.fastq -S $NOCCA_SAM

samtools view -Sb $NOCCA_SAM > $NOCCA_BAM

echo "Get read IDs of reads that map to tRNAs and use these to get two fastq files: from file which contains reads that had CCA trimmed, get reads that map to tRNAs; from file that contains reads that did not have CCA to be trimmed, get reads that do not map to tRNAs."
FINAL_FASTQ=${CLEANED_FASTQ%.fastq.gz}_tRNAs_trimmed.fastq
thomson_get_trna_bams_and_nontrna_reads.py --bam_no_cca $NOCCA_BAM --orig_fastq $CLEANED_FASTQ --no_cca_fastq ${CLEANED_FASTQ%.fastq.gz}_trimmed.fastq --output_fastq $FINAL_FASTQ --min-size 17 2>thomson_get_trna_bams_and_nontrna_reads-${NAME}.log --igenomes-dir $IGENOMES_DIR

echo "Map combined fastq files"
FINAL_SAM=${NAME}-k100N0_tRNAs_trimmed.sam
bowtie2 -p $CORES -t -q -N 0 -k 100 --end-to-end --sensitive -x $BOWTIE_INDEX -U $FINAL_FASTQ -S $FINAL_SAM

echo "Convert to bam and make multimapping info table"
samtools view -bS $FINAL_SAM | samtools sort - ${FINAL_SAM%.sam} 

thomson_multimapping_info_table.py --stranded -a ${FINAL_SAM%.sam}.bam -o ${NAME}_alignment_info.tsv --per-transcript-outfile ${NAME}_per_transcript_alignment_info.tsv --igenomes-dir $IGENOMES_DIR

#Remove sam if running low on space
#rm *sam

echo "Produce read length histograms"
if [ ! -e analyses ]; then
	mkdir analyses
fi
thomson_read_length_histogram.py -o analyses ${NAME}_alignment_info.tsv


