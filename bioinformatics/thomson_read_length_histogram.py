#!/usr/bin/env python
'''
Created on 22/04/2014
@author: katherine
'''
from argparse import ArgumentParser
from collections import Counter
from os import path

import matplotlib.pyplot as pl
import numpy as np
import pandas as pd
from sacgf.util import file_utils, csv_utils

#Because we are multimapping, it is not ok to just sum the read counts from each gene.
#Therefore, produce a plot for each biotype separately. 
# Each biotype plot contains the maximum number of reads that could map to that biotype.
#Reads may be represented more in different plots, but only once in a single plot.

def handle_args():
    parser = ArgumentParser(description='Collect stats on read length and biotype')
    parser.add_argument("-o", "--outdir", type=str,  help="Output directory")
    parser.add_argument("table")
    return parser.parse_args()

args = handle_args()

#Parameters
table_file = args.table
outdir = args.outdir
MAX_READ_LENGTH = 40
MIN_READ_LENGTH = 18
yrnas = set(['RNY1', 'RNY3', 'RNY4', 'RNY5'])
interesting_biotypes = ['yRNA', 'rRNA', 'snRNA', 'miRNA', 'snoRNA', 'tRNA']

#Setup
expt_name = file_utils.name_from_file_name(table_file)
graph_file = path.join(outdir, '%s.png' % expt_name)
csv_file = path.join(outdir, "%s.biotype_counts.csv" % expt_name)

table = pd.read_table(table_file)
pd.set_option("display.large_repr", "info")


def make_graph(graph_file, path, biotype, read_len_counter):
    '''Produce read length histogram for one biotype'''
    name_base, extension = path.splitext(graph_file)
    individual_graph_image = "%s.%s%s" % (name_base, biotype, extension) # extension still has dot
    
    #Fill gaps in counts, exclude too-short read data    
    counts = np.zeros(np.max(read_len_counter.keys()), dtype='i')
    for i in range(len(counts)):
        if i + 1 >= MIN_READ_LENGTH:
            counts[i] = read_len_counter[i + 1]
    
    y_label = "read counts"
    color = '#0000cd'
    width = 0.5
    xmin = MIN_READ_LENGTH - 1
    xmax = MAX_READ_LENGTH + 1
    largs = [i + 0.75 for i in range(len(counts))]
    
    pl.figure()
    pl.bar(largs, counts, width=width, color=color)
    pl.ylabel(y_label)
    pl.xlim(xmin=xmin, xmax=xmax)
    pl.savefig(individual_graph_image)
    pl.close()


biotype_table = table[pd.notnull(table['Gene_biotypes'])]

#Produce graph per biotype
all_read_lengths = set()
data = []
for biotype in interesting_biotypes:
    print biotype
    
    read_len_counter = Counter()
    for _, row in biotype_table.iterrows():
        count_this_sequence = False
        
        gene_biotypes = set(row['Gene_biotypes'].split(", "))
        
        if biotype == 'yRNA': #special case
            gene_names = set(row['Gene_names'].split(", "))
            
            number_of_yrnas = len(gene_names.intersection(yrnas))
            if number_of_yrnas >= 1:
                count_this_sequence = True
        
        elif biotype in gene_biotypes:
            count_this_sequence = True
        
        if count_this_sequence:
            #add # reads to read length counts
            read_length = row['Length(bp)']
            if read_length > MAX_READ_LENGTH:
                continue #skip these reads
                
            num_reads = row['Number_of_identical_reads']           
            read_len_counter[read_length] += num_reads
    
    #Plot graph of read length counts for biotype
    make_graph(graph_file, path, biotype, read_len_counter)
    
    #Prepare data to print to file
    all_read_lengths.update(read_len_counter.keys())
    read_len_counter['biotype'] = biotype
    data.append(read_len_counter)
   
# Write region stats CSV
labels = ['biotype'] + [i for i in all_read_lengths]
csv_utils.write_csv_dict(csv_file, labels, data)


